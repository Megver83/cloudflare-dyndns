## CloudFlare DDNS client
This script automatically updates your domain's IPv4 to match the running machine's IP.

Made to work with CloudFlare's API v4.

### Setup
#### Requirements:

* [curl](https://curl.haxx.se)
* `host` (from [bind](https://www.isc.org/software/bind/))
* [jq](http://stedolan.github.io/jq/)

In Arch/Parabola/Hyperbola you can install them with `pacman` like this:

    # pacman -S --needed bind-tools jq

(curl is already installed because it's a pacman's dependency)

#### Configuration:

Now edit the `cloudflare-ddns.conf` file and fill out the variables with your account's email and Global API Key (found in your profile's settings).

To configure domains, copy the `example.com.tld` file to `<your-domain>.tld` (the `.tld` extension is useful for the `for` loop when there are multiple domains). Edit the new file, and fill out with your domain's zone and subdomains (these are bash variables, so I made it easy to add subdomains in the "domains" variable).

Once you are done with all the domains you want, run:

    $ ./cloudflare-ddns-update.sh <your-domain>.tld

And it will update your configured domain. For multiple domains:

    $ for f in *.tld; do ./cloudflare-ddns-update.sh $f; done

### Cron
If you want to set up a cronjob to automatically update the DNS, here you have an example (this will be ran every five minutes):

    */5 * * * * cd /path/to/cloudflare-dyndns && ./cloudflare-ddns-update.sh <your-domain>.tld

Or for multiple domains:

    */5 * * * * cd /path/to/cloudflare-dyndns && for f in *.tld; do ./cloudflare-ddns-update.sh $f; done

## Credits
This script is based in this [GitHub Gist](https://gist.github.com/Tras2/cba88201b17d765ec065ccbedfb16d9a) and was modified to work with multiple domains and have an easier configuration.
